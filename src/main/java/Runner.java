
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import model.ApplicationsModel;
import model.AutorisationsModel;
import model.GeneralDataModel;
import model.MessageModel;
import model.ResultsModel;
import pojo.Application;
import pojo.Authorisation;
import pojo.Message;
import pojo.Results;


public class Runner {

		

	public static void main(String[] args) {

		Website website = Website.createCokies();
		ArrayList<Results> groups = null;
		ArrayList<HashMap<HashMap<String, String>, HashMap<String, ArrayList<String[]>>>> generalData = null;
		ArrayList<Authorisation> autorisations = null;
		ArrayList<Message> messages = null;
		ArrayList<Application> applications = null;
		ArrayList<Document> messageUrls = null;
		WritingToXML xmlWriter = new WritingToXML();
		for (int i = 1; i <= 16; i ++)
		{
			website = Website.grabGroupsFromPage(i);
			groups = getGroups(website.getDocument());

			for (Results group : groups)
			{
				System.out.println(group.getId());
				website = Website.grabDataFromURL(group.getLink());
				generalData = getGeneralData(website.getDocument());
				autorisations = getAutorisations(website.getDocument());
				messageUrls = new ArrayList<>();
				for (String messageUrl : getURLMessagesList(website.getDocument()))
				{
					messageUrls.add(Website.grabDataFromURL(messageUrl).getDocument());

				}
				messages = getMessages(messageUrls);
				applications = getApplications(website.getDocument());

				xmlWriter.writeGeneralDataMap(group,generalData, messages, autorisations, applications);
			}

		}
	}


	private static ArrayList<Results> getGroups(Document document) {
		ResultsModel resultModel = new ResultsModel();
		Elements table = document.select("table#searchSubstancesResults");
		Elements iteRow = table.select("tr");
		for (Element row : iteRow)
		{
			Elements tds = row.select("td");
			if (tds.size() > 0)
			{
				Element link = tds.get(3).select("a").first();
				String absHref = link.attr("abs:href");
				resultModel.addData(tds.get(0).text(), tds.get(1).text(), tds.get(2).text(),
						tds.get(3).text(), absHref, "1");
			}	
		}
		return resultModel.getResults();
	}

	private static ArrayList<HashMap<HashMap<String, String>, HashMap<String, ArrayList<String[]>>>> getGeneralData(Document document) {
		GeneralDataModel generalDataModel = new GeneralDataModel();
		Element table = document.select("table.form").first();
		Elements iteRow = table.select("tr");

		HashMap<String, String>  map = new LinkedHashMap<String, String>();
		HashMap<String, ArrayList<String[]>>  groupsMap = new LinkedHashMap<String, ArrayList<String[]>>();
		HashMap<HashMap<String, String>, HashMap<String, ArrayList<String[]>>> genealMap = new HashMap<>();
		ArrayList<String[]> groupsList = new ArrayList<>();
		String groupTh = null;
		String groupTd;

		for (int i = 0; i < iteRow.size(); i++)
		{
			for (Element element : iteRow.get(i).select("td"))
			{
				if (element.select("li").size() > 0)
				{
					for (int k = 0; k < element.select("li").size(); k++ )
					{
						groupTh = iteRow.get(i).select("th").text().trim().replaceAll(" ", "_").trim();
						groupTd = element.select("li").get(k).text().trim();
						String url = element.select("li").get(k).select("a").first().absUrl("href").replaceAll("&amp;", "&").trim();
						String tdSibling = "";
						if (!groupTd.startsWith("View list of all additives in this group"))
							tdSibling = element.select("li").get(k).select("a").first().nextSibling().toString();
						String[] array = new String[3];
						array[0] = groupTd.replaceAll("\\((.*?)\\)", "").trim();
						array[1] = url;
						array[2] = tdSibling.replace("(", "").replace(")", "").trim();
						groupsList.add(array);
					}
					groupsMap.put(groupTh, groupsList);
				}

				else if (element.text().length() > 2) {
					String elementTh = iteRow.get(i).select("th").text().replaceAll(" ", "_").trim();
					if (i == 0) //super fix
					{
						elementTh = elementTh.substring(0, elementTh.length() - 8);
					}

					String elementTd = element.text().trim();

					map.put(elementTh, elementTd);
				}

			}
		}
		genealMap.put(map, groupsMap);
		generalDataModel.addDataMap(genealMap);

		return generalDataModel.getMapResults();
	}


	private static ArrayList<Authorisation> getAutorisations(Document document) {
		AutorisationsModel generalDataModel = new AutorisationsModel();
		Element boxA = document.select("div.BoxA").first();
		Elements tables = boxA.select("table.form");
		ArrayList<String[]> categotyList;


		String categoryName, categoryUrl, categoryCode, legislation = "", legistlationUrl = "", applicableForm = "";

		//		String restrictionMaximumLimit, maximumLimitUnit, exception, footNotes1, description;

		for (int i= 0; i< tables.size(); i++) {

			Elements ul = tables.get(i).select("ul");
			Elements li = ul.select("li");
			categotyList = new ArrayList<>();
			for (int j = 0; j< li.size(); j++)
			{
				categoryName = li.get(j).select("a").first().text().replaceAll("\\((.*?)\\)", "").trim();
				categoryCode = li.get(j).select("a").first().text()
						.replaceAll("^[^(]*", "").replace("(", "").replace(")", "").trim();
				categoryUrl = li.get(j).select("a").first().absUrl("href").replaceAll("&amp;", "&").trim();
				if (li.get(j).select("a").size() > 1)
				{
					legislation = li.get(j).select("a").get(1).text().trim();
					legistlationUrl = li.get(j).select("a").get(1).absUrl("href").replaceAll("&amp;", "&").trim();

					applicableForm = li.get(j).select("a").get(1).nextSibling().toString().replaceAll("[,()a-z]", "").trim();	
				}
				String[] attributes = {categoryName, categoryCode, categoryUrl,
						legislation, legistlationUrl, applicableForm };
				categotyList.add(attributes);
			}

			ArrayList<String[]> restrictionList = new ArrayList<>();
			ArrayList<String[]> footnotesList = new ArrayList<>();
			String temp = null;
			for (int k = 0; k < tables.get(i).select("tr.even").size(); k ++)
			{
				if (tables.get(i).select("tr.even").get(k).select("th").first().text().length() > 1)
				{
					temp = tables.get(i).select("tr.even").get(k).select("th").first().text();
					if (tables.get(i).select("tr.even").get(k).select("td").size() > 1)
					{
						if (temp.startsWith("Footnotes"))
						{
							String[] footnotes = {tables.get(i).select("tr.even").get(k).select("td").get(0).text(),
									tables.get(i).select("tr.even").get(k).select("td").get(1).text()}; 
							footnotesList.add(footnotes);
						}
					}
					else
					{
						if (temp.startsWith("Individual restriction(s) / exception"))
						{
							String[] asd = tables.get(i).select("tr.even").get(k).select("td").first().text().split(";");
							String[] restrictionArray = {tables.get(i).select("tr.even").get(k).select("td").first().text()};
							restrictionList.add(asd);
						}
					}
				}

				if (tables.get(i).select("tr.even").get(k).select("th").first().text().length() < 2 &&
						!tables.get(i).select("tr.even").get(k).select("th").first().text().equals(temp)){
					if (tables.get(i).select("tr.even").get(k).select("td").size() > 1)
					{
						if (temp.startsWith("Footnotes"))
						{
							String[] footnotes = {tables.get(i).select("tr.even").get(k).select("td").get(0).text(),
									tables.get(i).select("tr.even").get(k).select("td").get(1).text()}; 
							footnotesList.add(footnotes);	
						}
					}
					else
					{
						if (temp.startsWith("Individual restriction(s) / exception"))
						{
							String[] asd = tables.get(i).select("tr.even").get(k).select("td").first().text().split(";");
							String[] restrictionArray = {tables.get(i).select("tr.even").get(k).select("td").first().text()};
							restrictionList.add(asd);
						}
					}
				}
				generalDataModel.addData(categotyList, restrictionList, footnotesList);
			}
		}
		return generalDataModel.getResults();
	}

	private static ArrayList<String> getURLMessagesList(Document document) {
		ArrayList<String> urlsMessageList = new ArrayList<>();
		Elements tables = null;
		for (Element box: document.select("form"))
		{
			if (box.text().startsWith("Messages"))
				tables = box.select("table.data");
		}

		if (tables != null)
		{
			for (int i= 0; i< tables.size(); i++) {

				Elements td = tables.get(i).select("td").select("a");
				for (int j = 0; j< td.size(); j++)
				{
					urlsMessageList.add(td.get(j).absUrl("href").replaceAll("&amp;", "&").trim());
				}
			}
		}

		return urlsMessageList;
	}

	private static ArrayList<Message> getMessages(ArrayList<Document> messageDocs) {
		MessageModel messageModel = new MessageModel();
		for (Document document : messageDocs )
		{
			Element boxA = document.select("div.BoxA").first();
			Elements tables = boxA.select("table.form");
			String message = "", reference = "", 	fileName = "", 	version = "", 	additionalInformation = "";
			for (int i= 0; i< tables.size(); i++) {
				Elements td = tables.get(i).select("td.even");
				for (int j = 0; j< td.size(); j++)
				{
					message = td.get(j).text();
				}
			}

			Elements tablesData = boxA.select("table.data");
			for (int i= 0; i< tablesData.size(); i++) {
				Elements td = tablesData.get(i).select("td");
				for (int j = 0; j < td.size(); j++)
				{
					td.get(j).text();
					if (j == 1)
						reference = td.get(j).text();
					if (j == 2)
						fileName = td.get(j).text();	
					if (j == 3)
						version = td.get(j).text();
					if (j == 4)
						additionalInformation = td.get(j).text();
				}
			}	
			messageModel.addData(message, reference, fileName, version, additionalInformation);
		}
		return messageModel.getResults();
	}

	private static ArrayList<Application> getApplications(Document document) {
		ApplicationsModel applicationsModel = new ApplicationsModel();
		Elements tables = document.select("table#tbl_applicationsForAuth.data");
		for (Element table : tables)
		{
			Elements tr = table.select("tr");
			for (Element trElement :  tr)
			{
				Elements td = trElement.select("td");
				if (td.size() > 0)
				{
					String legislationId = "";
					ArrayList<String> categotyList = new ArrayList<>();
					String refNO = td.get(1).text();			
					String date = td.get(3).text();
					String decision = td.get(4).text();
					String status = td.get(5).text();
					
					
					ArrayList<String[]> legislationList = new ArrayList<>();
					HashMap<String[], ArrayList[]> map = new LinkedHashMap<>();
					
					
					Elements liElements = td.get(2).select("li");
					for (int i = 0; i < liElements.size(); i++)
					{
						categotyList.add(liElements.get(i).text());
					}
					
					
					if (td.get(5).select("a").size() > 0)
					{
						String legislationStatusId = td.get(5).select("a").get(0).attr("href").
								replaceAll("[a-zA-Z'();:_]", "").split(",")[2].trim();
						Document legislations = Website.grabLegislationsFromURL("https://webgate.ec.europa.eu/sanco_foods/main/index.cfm?"
								+ "event=application.step4.summary&identifier=" + legislationStatusId ).getDocument();

						
						String actionforlegislationidentifier;
						String actionforlegislation;
						String category;
						String indivrestriction;
						String individualrestrictiontype;
						String individualrestrictionvalue;
						String individualrestrictionunit;
						String individualrestrictioncomments;
						String note_code;
						String note;

						
						Elements desisionItems = legislations.select("decision");
						
						
						for (int k = 0; k < desisionItems.size(); k ++)
						{
							
							ArrayList<String> desisionCategoriesList = new ArrayList<>();
							ArrayList<String[]> desisionRestList = new ArrayList<>();
							ArrayList<String[]> desisionNoteList = new ArrayList<>();
						
							actionforlegislationidentifier = desisionItems.get(k).select("actionforlegislationidentifier").first().text();
							actionforlegislation = desisionItems.get(k).select("actionforlegislation").first().text();
							
							String[] desionArray = {actionforlegislationidentifier, actionforlegislation};
							ArrayList[] groupOfAllAttributes = new ArrayList[3];
							
							Elements categories = desisionItems.get(k).select("category");

							for (int l = 0; l < categories.size(); l ++)
							{
								category = categories.get(l).select("category").first().text();
								desisionCategoriesList.add(category);
								
							}
							groupOfAllAttributes[0] = desisionCategoriesList;
							
							Elements indivrestrictions = desisionItems.get(k).select("indivrestriction");
							for (int g = 0; g < indivrestrictions.size(); g ++)
							{
								
								individualrestrictiontype = indivrestrictions.get(g).select("individualrestrictiontype").first().text();
								individualrestrictionvalue = indivrestrictions.get(g).select("individualrestrictionvalue").first().text();
								individualrestrictionunit = indivrestrictions.get(g).select("individualrestrictionunit").first().text();
								individualrestrictioncomments = indivrestrictions.get(g).select("individualrestrictioncomments").first().text();

								String[] individualRestr = {individualrestrictiontype, individualrestrictiontype, individualrestrictionunit, individualrestrictioncomments};
								desisionRestList.add(individualRestr);
								
							}
							groupOfAllAttributes[1] = desisionRestList;

							Elements notes_code = desisionItems.get(k).select("note_code");
							Elements notes = desisionItems.get(k).select("note");
							for (int j = 0; j < notes_code.size(); j ++)
							{
								note_code = notes_code.get(j).select("note_code").first().text();
								note = notes.get(j).select("note").first().text();
								String[] desisionNote = {note_code, note};
								desisionNoteList.add(desisionNote);
							}
							groupOfAllAttributes[2] = desisionNoteList;
							
							map.put(desionArray, groupOfAllAttributes);
						}	
					}

				
					if (td.get(6).select("a").size() > 0)
					{
						legislationId = td.get(6).select("a").get(0).attr("href").
								replaceAll("[javascript:_initPopupLegislativeReferences;]", "")
								.replace("(", "")
								.replace(")", "").trim();
						Document legislations = Website.grabLegislationsFromURL("https://webgate.ec.europa.eu/sanco_foods/main/index.cfm?event="
								+ "application.step5.categoriesLegislations&applicationIdentifier=" + legislationId ).getDocument();

						String legislationidentifier;
						String legislativereference;
						String publicationdate;
						String officialjournalpage;
						String effectiveAsOf;
						String officialjournalnumber;
						String categoryidentifier;
						String categoryname;
						String entryintoforcedate;
						String documentidentifier;
						Elements items = legislations.select("item");

						for (int k = 0; k < items.size(); k ++)
						{
							String[] legislationAttributes = new String[9];
							categoryidentifier = items.get(k).select("categoryidentifier").first().text();
							categoryname = items.get(k).select("categoryname").first().text();
							legislationidentifier = items.get(k).select("legislationidentifier").first().text();
							legislativereference = items.get(k).select("legislativereference").first().text();
							publicationdate = items.get(k).select("publicationdate").first().text();
							officialjournalpage = items.get(k).select("officialjournalpage").first().text();
							officialjournalnumber = items.get(k).select("officialjournalnumber").first().text();
							entryintoforcedate = items.get(k).select("entryintoforcedate").first().text();
							documentidentifier = items.get(k).select("documentidentifier").first().text();
							
							legislationAttributes[0] = categoryidentifier;
							legislationAttributes[1] = categoryname;
							legislationAttributes[2] = legislationidentifier;
							legislationAttributes[3] = legislativereference;
							legislationAttributes[4] = officialjournalnumber;
							legislationAttributes[5] = officialjournalpage;
							legislationAttributes[6] = publicationdate;
							legislationAttributes[7] = entryintoforcedate;
							legislationAttributes[8] = documentidentifier;
							legislationList.add(legislationAttributes);
						}
					}
					applicationsModel.addData(legislationId ,refNO, categotyList, date, decision, status, map, legislationList);	
				}		
			}
		}	

		return applicationsModel.getResults();
	}
}

