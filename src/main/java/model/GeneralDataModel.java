package model;
import java.util.ArrayList;
import java.util.HashMap;

import pojo.GeneralData;
import pojo.Results;

public class GeneralDataModel {

private ArrayList<HashMap<HashMap<String, String>, HashMap<String, ArrayList<String[]>>>> dataMap  = new ArrayList<>();
	
	public GeneralDataModel() 
	{
	}
	
	public ArrayList<HashMap<HashMap<String, String>, HashMap<String, ArrayList<String[]>>>> getMapResults()
	{
		return this.dataMap;
	}

	public void addDataMap(HashMap<HashMap<String, String>, HashMap<String, ArrayList<String[]>>> genealMap) {
		this.dataMap.add(genealMap);
	}


}
