package model;
import java.util.ArrayList;
import java.util.List;

import pojo.Results;

public class ResultsModel {
	
	private ArrayList<Results> data = new ArrayList<>();
	
	public ResultsModel() 
	{
	}
	
	public void addData(String id, String insNo, String eNo, String additiveName, String link, String pageNo)
	{
		this.data.add(new Results(id, insNo, eNo, additiveName, link, pageNo));
	}
	
	public ArrayList<Results> getResults()
	{
		return this.data;
	}

	
}
