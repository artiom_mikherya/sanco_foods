package model;

import java.util.ArrayList;
import java.util.HashMap;

import pojo.Application;
import pojo.Authorisation;

public class ApplicationsModel {
	
	private ArrayList<Application> data = new ArrayList<>();

	public ApplicationsModel() {
		
	}

	public void addData(String id, String refNO, ArrayList<String> categories, String date, String decision,
			String status, HashMap<String[], ArrayList[]> map, ArrayList<String[]> legistation)
	{	
		this.data.add(new Application(id, refNO, categories, date, decision,
				status, map, legistation));
	}
	
	public ArrayList<Application> getResults()
	{
		return this.data;
	}
}
