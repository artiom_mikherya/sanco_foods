package model;

import java.util.ArrayList;

import pojo.Authorisation;
import pojo.GeneralData;

public class AutorisationsModel {

private ArrayList<Authorisation> data = new ArrayList<>();
	
	public AutorisationsModel() 
	{
	}
	
	public ArrayList<Authorisation> getResults()
	{
		return this.data;
	}

	public void addData(ArrayList<String[]> categotyList, ArrayList<String[]> restrictionList,
			ArrayList<String[]> footnotesList) {
		this.data.add(new Authorisation(categotyList, restrictionList, footnotesList));
		
	}

}
