package model;

import java.util.ArrayList;

import pojo.Authorisation;
import pojo.Message;

public class MessageModel {

	private ArrayList<Message> data = new ArrayList<>();
	
	public MessageModel() {
	}

	public MessageModel(ArrayList<Message> data) {
		this.data = data;
	}

	public ArrayList<Message> getResults() {
		return this.data;
	}

	public void addData(String message, String reference, String fileName, String version, String additionalInformation) {
		this.data.add(new Message(message, reference, fileName, version, additionalInformation));
		
	}
	
	
}
