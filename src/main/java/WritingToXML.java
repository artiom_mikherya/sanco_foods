import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import pojo.Application;
import pojo.Authorisation;
import pojo.Message;
import pojo.Results;

public class WritingToXML 
{
	

	public void writeGeneralDataMap(Results group,
			ArrayList<HashMap<HashMap<String, String>, HashMap<String, ArrayList<String[]>>>> generalData, 
			ArrayList<Message> messages, ArrayList<Authorisation> autoristions, ArrayList<Application> applications) {
		SAXBuilder builder = new SAXBuilder();
		Document myDocument = null;
		Element rootElement = null;      
		String path = "groups.xml";

		// Appending to xml file 
		try        
		{  
			myDocument = builder.build(path); // checks for well-formed XML or not
			rootElement = myDocument.getRootElement();   
		} 
		catch(JDOMException dexp)     {
			  EventLog.log(Level.SEVERE, WritingToXML.class.getName(), "not a well formed document\n"+dexp.getMessage());
			   }
		catch(IOException e){ } 
		try
		{
			if(rootElement==null)          
				rootElement = new Element("Groups");

			if(myDocument==null)          
				myDocument = new Document(rootElement);

			Element groupId = new Element("group").setAttribute("id", group.getId());
			groupId.setAttribute("E_No", group.geteNo())
			.setAttribute("name", group.getAdditiveName())
			.setAttribute("url", group.getLink());

			for (HashMap<HashMap<String, String>, HashMap<String, ArrayList<String[]>>> result : generalData)
			{
				for (Map.Entry entries : result.entrySet())
				{
					HashMap<String, String> general = (HashMap<String, String>) entries.getKey();
					HashMap<String, ArrayList<String[]>> groupsList = (HashMap<String, ArrayList<String[]>>) entries.getValue();

					for (Map.Entry generalEntries : general.entrySet())
					{
						String generalElement = ((String) generalEntries.getKey()).replaceAll("\u00A0", "");;
						String generalContent = ((String) generalEntries.getValue()).replaceAll("\u00A0", "");;
						//						if (generalContent.startsWith("Yes"))
						//							generalContent = generalContent.substring(0, generalContent.length()-2);
						groupId.setAttribute(generalElement.trim(), generalContent.trim());
					}

					for (Map.Entry groupsListEntries : groupsList.entrySet())
					{
						String groupElement = (String) groupsListEntries.getKey();
						ArrayList<String[]> groupContent = (ArrayList<String[]>) groupsListEntries.getValue();
						Element compostionGroupElement = new Element(groupElement);
						groupId.addContent(compostionGroupElement);
						int i = 0;
						for (String[] composition : groupContent)
						{							
							compostionGroupElement.addContent(new Element("component")
									.setAttribute("id", String.valueOf(i).trim())
									.setAttribute("name", composition[0].trim())
									.setAttribute("code", composition[2].trim())
									.setAttribute("url", composition[1].trim()));
							i++;
						}
					}
				}	

				rootElement.addContent(groupId);
			}

			Element messagesElement = createMessagesElement(messages);
			groupId.addContent(messagesElement);

			Element autorisationsElement = createAutorisationsElement(autoristions);
			groupId.addContent(autorisationsElement);			

			Element applicationsElement = createApplicationsElement(applications);
			groupId.addContent(applicationsElement);			

			XMLOutputter outputter = new XMLOutputter();
			Format format = Format.getPrettyFormat();


			//			final XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat()) {
			//		        public String escapeElementEntities(String str) {
			//		            return str;
			//		        }
			//		    };
			outputter.setFormat(format);
			//			outputter.setXMLOutputProcessor(new CustomXMLOutputProcessor());
			//			outputter.output(myDocument, System.out);
			outputter.output(myDocument, new FileOutputStream(path));    
		}

		catch(IOException e)
		{
			 EventLog.log(Level.SEVERE, WritingToXML.class.getName(), e.getMessage());
		}

	}

	private Element createMessagesElement(ArrayList<Message> messages)
	{
		Element messagessElement = new Element("messages");
		int messageId = 0;
		for (Message message : messages)
		{
			Element messageElement = new Element("message");
			messagessElement.addContent(messageElement
					.setAttribute("id", String.valueOf(messageId))
					.setAttribute("label", message.getMessage()));
			messageId++;

			messageElement.addContent(new Element("attachments")
					.setAttribute("attachmentReference", message.getReference())
					.setAttribute("fileName", message.getFileName())
					.setAttribute("version", message.getVersion())
					.setAttribute("additionalInformation", message.getAdditionalInformation()));				
		}
		return messagessElement;
	}

	private Element createAutorisationsElement(ArrayList<Authorisation> autoristions) {
		Element autorisationsRootElement = new Element("authorisations");
		int i = 0;
		for (Authorisation autorisation : autoristions)
		{
			Element autorisationElement = new Element("authorisation").setAttribute("id", String.valueOf(i));
			i++;
			autorisationsRootElement.addContent(autorisationElement);

			Element categoryListElement = new Element("categories");
			autorisationElement.addContent(categoryListElement);
			int j = 0;

			for (String[] category : autorisation.getCategotyList())
			{
				categoryListElement.addContent(new Element("category")
						.setAttribute("id", String.valueOf(j))
						.setAttribute("name", category[0])
						.setAttribute("code", category[1])
						.setAttribute("legislation", category[3])
						.setAttribute("legistlationUrl", category[4])
						.setAttribute("applicableAsFrom", category[5])
						.setAttribute("url", category[2]));
				j++;
			}

			Element individulRestrictionRootElement = new Element("Individual_Restictions");
			int k = 0;
			for (String[] restriction : autorisation.getIndividualRestrictions())
			{
				Element restrictionElement = null;

				for (int g = 0; g < restriction.length; g ++)
				{
					restrictionElement = new Element("restriction");

					if (restriction[g].trim().equals("ML = quantum satis"))
					{
						restrictionElement.setAttribute("id", String.valueOf(k))
						.setAttribute("maximumLimit", "")
						.setAttribute("exeption", "quantum satis")
						.setAttribute("correction", "no");
					}	
					else if (restriction[g].trim().startsWith("except E"))
					{
						String[] splitByequals = restriction[g].split("=");
						String exeption = splitByequals[0].replaceAll("[^E0-9]", "").trim();
						String limit = splitByequals[1].replaceAll("[^0-9]", "").trim();
						String unit = splitByequals[1].replaceAll("[0-9]", "").trim();
						restrictionElement.setAttribute("id", String.valueOf(k))
						.setAttribute("maximumLimit", limit)
						.setAttribute("unit", unit)
						.setAttribute("exeption", exeption)
						.setAttribute("correction", "no");
					}
					//					<restriction range="E 620 to E 625" maximumLimit="10000" unit="mg/kg" individually="yes" inCombination="yes" expressedAs="glutamic acid" correction="no"/>
					else if (restriction[g].trim().indexOf("to E") >= 0){
						String[] spilt = restriction[g].split(",");
						String range = spilt[0].trim();
						String limit = spilt[1].replaceAll("[^0-9]", "").trim();
						String unit = spilt[1].replaceAll("[0-9]", "").trim();
						String individually = unit.indexOf("individually") >=0 ? "Yes" : "No";
						String combination = unit.indexOf("combination") >=0 ? "Yes" : "No";
						String expessedAs = spilt[2].replace("expressed", "").replace("as", "").trim();
						Pattern MY_PATTERN = Pattern.compile("[a-zA-Z]{2}[/][a-zA-Z]{2}");
						Matcher m = MY_PATTERN.matcher(unit);
						while (m.find()) {
							unit = m.group(0);
						}	
						restrictionElement.setAttribute("id", String.valueOf(k))
						.setAttribute("range", range)
						.setAttribute("maximumLimit", limit)
						.setAttribute("maximumLimitUnit", unit)
						.setAttribute("individually", individually)
						.setAttribute("inCombination", combination)
						.setAttribute("expressedAs", expessedAs)
						.setAttribute("correction", "no");	
					}
					else if(restriction[g].trim().indexOf("mg/") >= 0) {
						String[] splitBycomma = restriction[g].trim().split(",");
						String limit = splitBycomma[0].split("=")[1].replaceAll("[^0-9]", "").trim();
						String unit = splitBycomma[0].split("=")[1].replaceAll("[0-9]", "").trim();
						StringBuilder builder = new StringBuilder();
						for(int l = 0; l < splitBycomma.length; l++) {
							if (l > 0)
							{
								builder.append(splitBycomma[l]);
								if (l != splitBycomma.length -1 )
									builder.append(",");
							}
						}

						restrictionElement.setAttribute("id", String.valueOf(k))
						.setAttribute("maximumLimit", limit)
						.setAttribute("maximumLimitUnit", unit)
						.setAttribute("exception", builder.toString().trim())
						.setAttribute("description", "");	
					}

					else {
						restrictionElement.setAttribute("id", String.valueOf(k))
						.setAttribute("maximumLimit", "")
						.setAttribute("maximumLimitUnit", "")
						.setAttribute("exception", restriction[g])
						.setAttribute("description", "");	
					}

					k++;
					individulRestrictionRootElement.addContent(restrictionElement);
				}

			}

			for (String[] footNotes : autorisation.getFootNotes())
			{
				Element footNotesElement = new Element("footnote")
						.setAttribute("id", footNotes[0])
						.setAttribute("content", footNotes[1]);
				individulRestrictionRootElement.addContent(footNotesElement);

			}
			autorisationElement.addContent(individulRestrictionRootElement);
		}

		return autorisationsRootElement;
	}

	private Element createApplicationsElement(ArrayList<Application> applications) {
		Element applicationsRootElement = new Element("applications");
		for (Application application : applications)
		{
			Element applicationElement = new Element("application");
			applicationsRootElement.addContent(applicationElement
					.setAttribute("referenceNumber", application.getApplicationrefNo())
					.setAttribute("applicationDate", application.getDate())
					.setAttribute("AAPDecision", application.getAapDesision())
					.setAttribute("statustatus", application.getApplictionStatus())
					.setAttribute("legistlationUrl", "https://webgate.ec.europa.eu/sanco_foods/main/index.cfm?event="
								+ "application.step5.categoriesLegislations&applicationIdentifier=" + application.getId()));


			Element categoryListElement = new Element("categories");
			applicationElement.addContent(categoryListElement);
			int j = 0;
			for (String category : application.getCategories())
			{
				categoryListElement.addContent(new Element("category").addContent(category)
						.setAttribute("id", String.valueOf(j)));
				j++;
			}

			Element desisionsElements = new Element("desisions");
			applicationElement.addContent(desisionsElements);
			int k = 0;
			for (Map.Entry<String[], ArrayList[]> entry : application.getMap().entrySet())
			{

				String[] entryKey = entry.getKey();
				ArrayList[] entryValue = entry.getValue();

				ArrayList<String> categories = entryValue[0];
				ArrayList<String[]> restrictions = entryValue[1];
				ArrayList<String[]> notes = entryValue[2];

				String actionforlegislationidentifier = entryKey[0];
				String actionforlegislation = entryKey[1];;

				Element desionElement = new Element("desision");
				desionElement.setAttribute("actionforlegislationidentifier", actionforlegislationidentifier);
				desionElement.setAttribute("actionforlegislation", actionforlegislation);

				Element categoriesElement = new Element("categories");
				desionElement.addContent(categoriesElement);

				for (String cat : categories)
				{
					Element categoryElement = new Element("category");
					categoriesElement.addContent(categoryElement);
					categoryElement.setAttribute("description", cat);
				}


				Element restrictionsElement = new Element("restrictions");
				desionElement.addContent(restrictionsElement);
				for (String[] restr : restrictions)
				{
					Element restrictionElement = new Element("restriction");
					restrictionsElement.addContent(restrictionElement);
					restrictionElement.setAttribute("individualrestrictiontype", restr[0])
					.setAttribute("individualrestrictionvalue", restr[1])
					.setAttribute("individualrestrictionunit", restr[2])
					.setAttribute("individualrestrictioncomments", restr[3]);
				}

				Element footNotesElement = new Element("footenotes");
				desionElement.addContent(footNotesElement);
				for (String[] note : notes)
				{
					Element footNoteElement = new Element("footnote");
					footNotesElement.addContent(footNoteElement);
					footNoteElement.setAttribute("note_code", note[0])
					.setAttribute("note", note[1]);
				}


				desisionsElements.addContent(desionElement.setAttribute("id", String.valueOf(k)));
				k++;
			}


			Element legislations = new Element("legislations");
			applicationElement.addContent(legislations);
			for (String[] legi : application.getLegislation())
			{
				legislations.addContent(new Element("legislation")
						.setAttribute("categoryidentifier", legi[0])
						.setAttribute("categoryname", legi[1])
						.setAttribute("legislationidentifier", legi[2])
						.setAttribute("legislativereference", legi[3])
						.setAttribute("officialjournalnumber", legi[4])
						.setAttribute("officialjournalpage", legi[5])
						.setAttribute("publicationdate", legi[6])
						.setAttribute("entryintoforcedate", legi[7])
						.setAttribute("documentidentifier", legi[8]));					
			}
		}
		return applicationsRootElement;
	}

}