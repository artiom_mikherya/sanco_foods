package multithread;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

public class GrabPage implements Callable<GrabPage> {
	static final int TIMEOUT = 60000;   // one minute

	private URL url;
	private int depth;
	private Set<URL> urlList = new HashSet<>();

	public GrabPage(URL url, int depth) {
		this.url = url;
		this.depth = depth;
	}

	@Override
	public GrabPage call() throws Exception {
		Document document = null;
		System.out.println("Visiting (" + depth + "): " + url.toString());
		document = Jsoup.parse(url, TIMEOUT);

		processLinks(document.select("a[href]"));

		return this;
	}

	private void processLinks(Elements links) {
		for (Element link : links) {
			String href = link.attr("href");
			if (href.isEmpty()
			||  href.startsWith("#")) {
				continue;
			}

			try {
				URL nextUrl = new URL(url, href);
				urlList.add(nextUrl);
			} catch (MalformedURLException e) { // ignore bad urls
			}
		}
	}

	public Set<URL> getUrlList() {
		return urlList;
	}

	public int getDepth() {
		return depth;
	}
}