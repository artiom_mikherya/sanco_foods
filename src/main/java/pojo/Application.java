package pojo;
import java.util.ArrayList;
import java.util.HashMap;

public class Application {

	private String applicationrefNo, date, aapDesision, applictionStatus, id;
	
	private ArrayList<String> categories;  
	private ArrayList<String[]> legislation;

	private HashMap<String[], ArrayList[]> map;
	

	
	public Application(String id, String applicationrefNo, ArrayList<String> categories, String date, String decision,
			String status, HashMap<String[], ArrayList[]> map, ArrayList<String[]> legistation){

		this.applicationrefNo = applicationrefNo;
		this.date = date;
		this.categories = categories;
		this.aapDesision = decision;
		this.applictionStatus = status;
		this.legislation = legistation;
		this.map = map;
		this.setId(id);

	}
	
	
	public String getApplicationrefNo() {
		return applicationrefNo;
	}
	public void setApplicationrefNo(String applicationrefNo) {
		this.applicationrefNo = applicationrefNo;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public ArrayList<String> getCategories() {
		return categories;
	}
	public void setCategories(ArrayList<String> categories) {
		this.categories = categories;
	}
	public String getApplictionStatus() {
		return applictionStatus;
	}
	public void setApplictionStatus(String applictionStatus) {
		this.applictionStatus = applictionStatus;
	}
	public ArrayList<String[]> getLegislation() {
		return legislation;
	}
	public void setLegislation(ArrayList<String[]> legislation) {
		this.legislation = legislation;
	}
	public String getAapDesision() {
		return aapDesision;
	}
	public void setAapDesision(String aapDesision) {
		this.aapDesision = aapDesision;
	}


	public HashMap<String[], ArrayList[]> getMap() {
		return map;
	}


	public void setMap(HashMap<String[], ArrayList[]> map) {
		this.map = map;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}
	
}
