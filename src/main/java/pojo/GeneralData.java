package pojo;
import java.util.ArrayList;

public class GeneralData {


	private String internalRefNo, eNo, insNo;
	private String additiveName, synonymName;
	private ArrayList<String> componentOfTheGroup;
	private String group;
	
	
	public GeneralData(String internalRefNo, String additiveName, String synonymName, String eNo, String insNo, String group, 
			ArrayList<String> componentOfTheGroup)
	{
		this.internalRefNo = internalRefNo;
		this.eNo = eNo;
		this.additiveName = additiveName;
		this.synonymName = synonymName;
		this.componentOfTheGroup = componentOfTheGroup;
		this.group = group;
		this.setInsNo(insNo);
		
	}
	
	public String getInternalRefNo() {
		return internalRefNo;
	}
	
	public void setInternalRefNo(String internalRefNo) {
		this.internalRefNo = internalRefNo;
	}
	public String geteNo() {
		return eNo;
	}
	public void seteNo(String eNo) {
		this.eNo = eNo;
	}
	public String getSynonymName() {
		return synonymName;
	}
	public void setSynonymName(String synonymName) {
		this.synonymName = synonymName;
	}
	public String getAdditiveName() {
		return additiveName;
	}
	public void setAdditiveName(String additiveName) {
		this.additiveName = additiveName;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String hasIngroup) {
		this.group = hasIngroup;
	}
	public ArrayList<String> getComponentOfTheGroup() {
		return componentOfTheGroup;
	}
	public void setComponentOfTheGroup(ArrayList<String> componentOfTheGroup) {
		this.componentOfTheGroup = componentOfTheGroup;
	}

	public String getInsNo() {
		return insNo;
	}

	public void setInsNo(String insNo) {
		this.insNo = insNo;
	}
}
