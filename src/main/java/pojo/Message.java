package pojo;

public class Message {
	
	private String message, reference, 	fileName, 	version, 	additionalInformation;

	public Message(String message, String reference, String fileName, String version, String additionalInformation)
	{
		this.message = message;
		this.reference = reference;
		this.fileName = fileName;
		this.version = version;
		this.additionalInformation = additionalInformation;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
}
