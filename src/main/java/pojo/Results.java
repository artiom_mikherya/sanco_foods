package pojo;

import java.util.ArrayList;
import java.util.List;

public class Results {

	private String id, insNo, pageNo;
	private String eNo, additiveName;
	private String link;



	public Results(String id, String insNo, String eNo, String additiveName, String link, String pageNo) 
	{
		this.id = id;
		this.insNo = insNo;
		this.eNo = eNo;
		this.additiveName = additiveName;
		this.link = link;
		this.pageNo = pageNo;
		
	}


	public String getPageNo() {
		return pageNo;
	}


	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}



	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getAdditiveName() {
		return additiveName;
	}


	public void setAdditiveName(String additiveName) {
		this.additiveName = additiveName;
	}


	public String geteNo() {
		return eNo;
	}


	public void seteNo(String eNo) {
		this.eNo = eNo;
	}


	@Override
	public String toString() {
		return "PageNo " + pageNo;
	}


	public String getInsNo() {
		return insNo;
	}


	public void setInsNo(String insNo) {
		this.insNo = insNo;
	}


	public String getLink() {
		return this.link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
