package pojo;
import java.util.ArrayList;

public class Authorisation {

	private ArrayList<String[]> categotyList;
	private ArrayList<String[]> footNotes, individualRestrictions;
	
	
//	 String name, code, legislation, legistlationUrl;
//	String restrictionMaximumLimit, maximumLimitUnit, exception, footNotes1, description;

	
	
	public Authorisation()
	{
	}
	
	
	public Authorisation(ArrayList<String[]> categotyList, ArrayList<String[]> individualRestrictions,ArrayList<String[]> footNotes )
	{
		this.categotyList = categotyList;
		this.individualRestrictions = individualRestrictions;
		this.footNotes = footNotes;
	}
	

	public ArrayList<String[]> getCategotyList() {
		return categotyList;
	}
	
	public void setCategotyList(ArrayList<String[]> categotyList) {
		this.categotyList = categotyList;
	}
	
	public ArrayList<String[]> getFootNotes() {
		return footNotes;
	}
	
	public void setFootNotes(ArrayList<String[]> footNotes) {
		this.footNotes = footNotes;
	}
	public ArrayList<String[]> getIndividualRestrictions() {
		return individualRestrictions;
	}
	public void setIndividualRestrictions(ArrayList<String[]> individualRestrictions) {
		this.individualRestrictions = individualRestrictions;
	}
	
}

