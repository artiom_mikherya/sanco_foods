

import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.Map;
import java.util.logging.Level;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Website {

	/**
	 * Represents the default url
	 */
	private static final String URL = "https://webgate.ec.europa.eu/sanco_foods/main/?event=substances.search&substances.pagination=";

	private static Map<String, String> loginCookies = null;


	public static Website createCokies() {
		Connection.Response res;
		try {
			res = Jsoup.connect("https://webgate.ec.europa.eu/sanco_foods/main/?sector=FAD&auth=SANCAS").execute();
			loginCookies = res.cookies();
			EventLog.log(Level.INFO, Website.class.getName(), "logged in with session ID" + loginCookies.get("JSESSIONID"));

		} 
		catch (SocketTimeoutException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (ConnectException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (MalformedURLException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (IOException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}

		return null;

	}

	public static Map<String, String> getLoginCookies() {
		return loginCookies;
	}


	public static void setLoginCookies(Map<String, String> loginCookies) {
		Website.loginCookies = loginCookies;
	}



	/**
	 * Represents the local document of the website
	 */
	private final Document document;

	/**
	 * Constructs the cinemark website
	 * @param document	the website document
	 */
	private Website(final Document document) {
		this.document = document;
	}


	public Document getDocument()
	{		
		return this.document;
	}

	/**
	 * Creates an instance of the {@link Website} used for refference and lookup
	 * @param page
	 * @return	the groups list
	 */
	public static final Website grabGroupsFromPage(int page) {
		try {
			return new Website(Jsoup.connect(URL + page)
					.userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6").cookies(loginCookies).get());

		}
		catch (SocketTimeoutException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (ConnectException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (MalformedURLException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (IOException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}

		return null;
	}

	/**
	 * Creates an instance of the {@link Website} used for refference and lookup
	 * @param url
	 * @return	document
	 */
	public static final Website grabDataFromURL(String url) {
		try {
			EventLog.log(Level.INFO, Website.class.getName(), url);
			return new Website(Jsoup.connect(url)
					.userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6").cookies(loginCookies).get());

		} catch (SocketTimeoutException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (ConnectException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (MalformedURLException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (IOException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}

		return null;
	}

	/**
	 * Creates an instance of the {@link Website} used for refference and lookup
	 * @param url 
	 * @return	document
	 */
	public static final Website grabLegislationsFromURL(String url) {
		try {
			EventLog.log(Level.INFO, Website.class.getName(), url);
			//			  log.trace("Response {}: {}", resp.statusCode(), resp.statusMessage());
			//			  if ( resp.statusCode() != 200 )
			//			  {
			//			    for ( final Map.Entry<String, String> entry : resp.headers().entrySet() )
			//			      log.trace("Header {}: {}", entry.getKey(), entry.getValue());
			return new Website(Jsoup.connect(url)
					.userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6").cookies(loginCookies).get());

		}
		catch (SocketTimeoutException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (ConnectException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (MalformedURLException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (IOException e) {
			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}

		return null;
	}

}
