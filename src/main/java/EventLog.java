import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EventLog {

	 static Logger logger;
	    public Handler fileHandler;
	    Formatter plainText;

	public EventLog() throws IOException {
        logger = Logger.getLogger(EventLog.class.getName());
        fileHandler = new FileHandler("event.log",true);
        plainText = new CustomFormatter();
        fileHandler.setFormatter(plainText);
        logger.addHandler(fileHandler);

    }
	
	private static Logger getLogger(){
	    if(logger == null){
	        try {
	            new EventLog();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return logger;
	}
	
	public static void log(Level level, String className, String msg){
	    getLogger().log(level, className + " : " + msg);
	}
}