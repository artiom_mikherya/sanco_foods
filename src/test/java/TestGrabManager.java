

import org.junit.Test;

import multithread.GrabManager;

import java.io.IOException;
import java.net.URL;

public class TestGrabManager {
	@Test
	public void happy() throws IOException, InterruptedException {
		GrabManager grabManager = new GrabManager(2, 50);
		grabManager.go(new URL("http://news.yahoo.com"));
		grabManager.write("urllist.txt");
	}

}